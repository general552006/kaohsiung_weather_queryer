'use strict'

import inquirer from 'inquirer';
import axios from 'axios';

// 政府資料開放平臺中央氣象局api授權碼
const openDataAuthorization = 'rdec-key-123-45678-011121314';
// 政府資料開放平臺中央氣象局高雄市天氣預報api
const openDataApiUrl = 'https://opendata.cwb.gov.tw/api/v1/rest/datastore/F-D0047-065';

// 高雄市行政區陣列
const district = [
  '鹽埕區',
  '鼓山區',
  '左營區',
  '楠梓區',
  '三民區',
  '新興區',
  '前金區',
  '苓雅區',
  '前鎮區',
  '旗津區',
  '小港區',
  '鳳山區',
  '林園區',
  '大寮區',
  '大樹區',
  '大社區',
  '仁武區',
  '鳥松區',
  '岡山區',
  '橋頭區',
  '燕巢區',
  '阿蓮區',
  '路竹區',
  '湖內區',
  '茄萣區',
  '梓官區',
  '旗山區',
  '美濃區',
  '六龜區',
  '甲仙區',
  '杉林區',
  '內門區',
  '茂林區',
  '桃源區',
  '那瑪夏區',
  '田寮區',
  '永安區',
  '彌陀區'
];

// 問題陣列參數
const questions = [
  {
    type: 'list',
    name: 'district',
    message: '請選擇高雄市行政區：',
    choices: district
  }
];

/**
 * 取得政府資料開放平臺高雄市行政區資料方法
 * @param {array} districtArray 行政區陣列
 * 
 * 
 * @returns {object} response
 */
let getWeatherData = async (districtArray) => {
  let response = {};

  await axios.get(openDataApiUrl, {
    params: {
      Authorization: openDataAuthorization,
      locationName: districtArray.toString()
    }
  })
  .then(function (res) {
    if (res.data.success) {
      response = res.data;
    }
  })
  .catch(function (err) {
    console.log(err);
  });
  return response
}


/**
 * 主程序
 */
let  main = () => {
  inquirer.prompt(questions).then(async (answers) => {
    let response = await getWeatherData([answers.district]);
    let records = response.records;

    records.locations[0].location[0].weatherElement.map((row) => {
      if (row.elementName == 'WeatherDescription') return;

      let message = '';
      let time = row.time;
      console.log(`------------------------ ${row.description} ------------------------`);
      switch(row.elementName) {
        case 'PoP12h':
        case 'PoP6h':
          time.map((timeRow) => {
            message = `時間：${timeRow.startTime}至${timeRow.endTime} ${timeRow.elementValue[0].value}%`;
            console.log(message);
          });
          break;
        case 'AT':
        case 'T':
        case 'Td':
          time.map((timeRow) => {
            message = `時間：${timeRow.dataTime} ${timeRow.elementValue[0].measures}${timeRow.elementValue[0].value}度`;
            console.log(message);
          });
          break;
        case 'RH':
          time.map((timeRow) => {
            message = `時間：${timeRow.dataTime} ${timeRow.elementValue[0].value}%`;
            console.log(message);
          });
          break;
        case 'CI':
          time.map((timeRow) => {
            message = `時間：${timeRow.dataTime} ${timeRow.elementValue[0].value}(${timeRow.elementValue[1].value})`;
            console.log(message);
          });
          break;
        case 'Wx':
          time.map((timeRow) => {
            message = `時間：${timeRow.startTime}至${timeRow.endTime} ${timeRow.elementValue[0].value}`;
            console.log(message);
          });
          break;
        case 'WS':
          time.map((timeRow) => {
            message = `時間：${timeRow.dataTime} ${timeRow.elementValue[0].value}${timeRow.elementValue[0].measures}`;
            console.log(message);
          });
          break;
        case 'WD':
          time.map((timeRow) => {
            message = `時間：${timeRow.dataTime} ${timeRow.elementValue[0].measures}${timeRow.elementValue[0].value}`;
            console.log(message);
          });
          break;
      };
    });
  }).catch((error) => {
    console.log(error);
  });
}

// 執行主程序
main();

